//
//  Vote.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/1/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import Foundation
import CoreData

@objc(Vote)
class Vote: NSManagedObject {

    @NSManaged var car: Car!
    @NSManaged var competition: Competition!

}
