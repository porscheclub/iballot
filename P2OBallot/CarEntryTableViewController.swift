//
//  CarEntryTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class CarEntryTableViewController: UITableViewController, UITableViewDelegate {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext?
    var competition: Competition?
    
    var hasSelectedCar: Bool = false
    var selectedCar: Car?
    
    var sortedCars: NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortedCars = self.competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
    }
    
    override func viewWillAppear(animated: Bool) {
        self.sortedCars = self.competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return self.sortedCars.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("reuseableCell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "reusableCell")
        }
        
        // Retrieve the instance that this is referring to.
        let car: Car = self.sortedCars.objectAtIndex(indexPath.row) as Car
        
        if car.lastName != nil && car.firstName != nil && car.lastName != "" && car.firstName != "" {
            cell!.textLabel?.text = "\(car.number) - \(car.lastName!), \(car.firstName!)"
        } else {
            cell!.textLabel?.text = "\(car.number)"
        }
        
        if car.year != nil && car.year != 0 && car.model != nil && car.model != "" {
            cell!.detailTextLabel?.text = "\(car.model!), \(car.year!)"
        } else {
            cell!.detailTextLabel?.text = ""
        }
        cell!.accessoryType = .DisclosureIndicator
        
        return cell!
    }
    
    // MARK: - Events
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Get the row that was selected.
        let cars: NSArray = self.competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
        self.selectedCar = cars.objectAtIndex(indexPath.row) as? Car
        self.hasSelectedCar = true
        
        self.performSegueWithIdentifier("showNewCarForm", sender: self)
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        var error: NSError?
        if !self.context!.save(&error) {
            let alert: UIAlertController = UIAlertController(
                title: "Error Saving",
                message: "There was an error saving the competition data.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showNewCarForm" {
            let navController: UINavigationController = segue.destinationViewController as UINavigationController
            let controller: NewCarViewController = navController.topViewController as NewCarViewController
            controller.context = self.context
            if let car = self.selectedCar {
                if hasSelectedCar {
                    controller.car = car
                    hasSelectedCar = false
                }
            }
        }
    }
}
