//
//  NewContestViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class NewContestViewController: FXFormViewController {
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var continueButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext?
    var myForm: NewContestForm = NewContestForm()
    var competition: Competition?
    
    var carMin: Int!
    var carMax: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formController.form = self.myForm
        
        // Retrieve a reference to the managed object context.
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        self.context = appDelegate.managedObjectContext
    }
    
    override func viewWillAppear(animated: Bool) {
        // Handle the case where there's already a competition active.
        if let comp = self.competition {
            let cars: NSArray = comp.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
            
            self.myForm.competitionName = comp.name
            self.myForm.location = comp.location
            self.myForm.about = comp.about
            self.myForm.carMin = "\((cars.firstObject! as Car).number.integerValue)"
            self.myForm.carMax = "\((cars.lastObject! as Car).number.integerValue)"
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showCarEntryTable" {
            let navController: UINavigationController = segue.destinationViewController as UINavigationController
            let controller: CarEntryTableViewController = navController.topViewController as CarEntryTableViewController
            controller.context = self.context
            
            var isNew: Bool = false
            
            if self.competition == nil {
                // TODO: De-activate other competitions.
                let description: NSEntityDescription = NSEntityDescription.entityForName("Competition", inManagedObjectContext: self.context!)!
                let request: NSFetchRequest = NSFetchRequest()
                request.entity = description
                let predicate: NSPredicate = NSPredicate(format: "isActive == %@", NSNumber(bool: true))!
                request.predicate = predicate
                var error: NSError?
                let array: NSArray = self.context!.executeFetchRequest(request, error: &error)!
                if array.count > 0 {
                    for obj in array {
                        let comp: Competition? = obj as? Competition
                        comp?.isActive = false
                    }
                }
                
                self.competition = NSEntityDescription.insertNewObjectForEntityForName(
                    "Competition",
                    inManagedObjectContext: self.context!) as? Competition
                
                competition!.startDate = NSDate()
                competition!.isActive = true
                
                isNew = true
            }
            
            competition!.name = myForm.competitionName
            competition!.about = myForm.about
            competition!.location = myForm.location
            
            let cars: NSMutableSet = competition!.mutableSetValueForKey("cars")
            if isNew {
                self.carMin = myForm.carMin.toInt()!
                self.carMax = myForm.carMax.toInt()!
                for number in self.carMin...self.carMax {
                    let newCar: Car = NSEntityDescription.insertNewObjectForEntityForName(
                        "Car",
                        inManagedObjectContext: self.context!) as Car
                    newCar.number = NSNumber(integer: number)
                    newCar.competition = competition!
                    cars.addObject(newCar)
                }
                
                controller.competition = competition
            } else {
                // Delete any out-of-range cars
                if self.carMin < myForm.carMin.toInt()! {
                    let sorted: NSArray = competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
                    var it: Int = self.carMin
                    while it < myForm.carMin.toInt()! {
                        let car: Car = sorted.objectAtIndex(it - self.carMin) as Car
                        cars.removeObject(car)
                        car.competition = nil
                        self.context!.deleteObject(car)
                        
                        it++
                    }
                }
                
                var iter: Int = myForm.carMin.toInt()!
                
                // Create new cars at the beginning of the range.
                while iter < self.carMin {
                    let newCar: Car = NSEntityDescription.insertNewObjectForEntityForName(
                        "Car",
                        inManagedObjectContext: self.context!) as Car
                    newCar.number = NSNumber(integer: iter)
                    newCar.competition = competition!
                    cars.addObject(newCar)
                    
                    iter++
                }
                
                // Skip the overlap
                while iter <= self.carMax && iter <= myForm.carMax.toInt()! { iter++ }
                
                // Handle the end of the range
                if self.carMax < myForm.carMax.toInt()! {
                    while iter <= myForm.carMax.toInt()! {
                        let newCar: Car = NSEntityDescription.insertNewObjectForEntityForName(
                            "Car",
                            inManagedObjectContext: self.context!) as Car
                        newCar.number = NSNumber(integer: iter)
                        newCar.competition = competition!
                        cars.addObject(newCar)
                        
                        iter++
                    }
                } else {
                    let sorted: NSArray = competition!.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
                    while iter <= self.carMax {
                        var car: Car = sorted.objectAtIndex(iter - myForm.carMin.toInt()!) as Car
                        cars.removeObject(car)
                        car.competition = nil
                        self.context!.deleteObject(car)
                        
                        iter++
                    }
                }
                
                self.carMin = myForm.carMin.toInt()!
                self.carMax = myForm.carMax.toInt()!
                controller.competition = competition
            }
        }
    }
    
    @IBAction func onCancelSelected(sender: AnyObject) {
        self.context?.rollback()
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onContinueSelected(sender: AnyObject) {
        // Validate the data before performing the segue.
        if myForm.competitionName == nil || myForm.competitionName == "" {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Name",
                message: "Your competition must have a name.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.about == nil || myForm.about == "" {
            let alert: UIAlertController = UIAlertController(
                title: "No Description",
                message: "Your competition must have an about section.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.location == nil || myForm.location == "" {
            let alert: UIAlertController = UIAlertController(
                title: "No Location",
                message: "The competition location cannot be empty.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.carMin == nil || myForm.carMin.toInt() == nil || myForm.carMin.toInt()! < 0 || myForm.carMin.toInt()! > 998 {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Minimum Car Number",
                message: "The competition must have a minimum car number.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.carMax == nil || myForm.carMax.toInt() == nil || myForm.carMax.toInt()! < 1 || myForm.carMax.toInt()! > 999 {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Maximum Car Number",
                message: "The competition must have a maximum car number.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else if myForm.carMax.toInt()! <= myForm.carMin.toInt()! {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Car Range",
                message: "The provided range of car numbers is invalid.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            self.performSegueWithIdentifier("showCarEntryTable", sender: self)
        }
    }
}
