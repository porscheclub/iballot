//
//  NewAdminLoginViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/18/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit

class AdminLoginViewController: UIViewController, THPinViewControllerDelegate {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    var isRegistered: Bool = false
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Reset all of the data.
        usernameField.text = ""
        passwordField.text = ""
        
        // Configure the layouts based on the known admin information.
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let username = defaults.stringForKey(DEFAULTS_USERNAME) {
            if let password = defaults.stringForKey(DEFAULTS_PASSWORD) {
                setUpRegisteredUI()
                self.isRegistered = true
            } else {
                setUpUnregisteredUI()
                self.isRegistered = false
            }
        } else {
            setUpUnregisteredUI()
            self.isRegistered = false
        }
    }
    
    func setUpUnregisteredUI() {
        loginButton.setTitle("Create >", forState: UIControlState.Normal)
        textLabel.text = "No admin account exists, please create a new one."
    }
    
    func setUpRegisteredUI() {
        loginButton.setTitle("Login >", forState: UIControlState.Normal)
        textLabel.text = "Please enter your login credentials."
    }
    
    /*
     * Admin override logic ========================================================================
     */
    
    // Launch the admin override when the device is shaken from this screen.
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent) {
        if motion == UIEventSubtype.MotionShake {
            var controller: THPinViewController = THPinViewController(delegate: self)
            controller.promptTitle = "Enter the Admin Override PIN"
            controller.promptColor = UIColor.orangeColor()
            controller.view.tintColor = UIColor.orangeColor()
            controller.hideLetters = true
            self.view.tag = 14742
            self.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
            controller.translucentBackground = true
            
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    func pinLengthForPinViewController(pinViewController: THPinViewController!) -> UInt {
        return UInt(4)
    }
    
    func pinViewController(pinViewController: THPinViewController!, isPinValid pin: String!) -> Bool {
        return pin == "1931"
    }
    
    func userCanRetryInPinViewController(pinViewController: THPinViewController!) -> Bool {
        return true
    }
    
    func pinViewControllerWillDismissAfterPinEntryWasSuccessful(pinViewController: THPinViewController!) {
        // Pin entry was successful, reset the admin data.
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(DEFAULTS_USERNAME)
        defaults.removeObjectForKey(DEFAULTS_PASSWORD)
    }
    
    /*
     * Admin override logic ========================================================================
     */
    
    @IBAction func onLoginPressed(sender: AnyObject) {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let username: String = self.usernameField.text
        let password: String = self.passwordField.text
        
        if self.isRegistered {
            // Validate the admin credentials.
            let valUsername: String! = defaults.stringForKey(DEFAULTS_USERNAME)
            let valPassword: String! = defaults.stringForKey(DEFAULTS_PASSWORD)
            
            if username != valUsername {
                let alert: UIAlertView = UIAlertView(
                    title: "Invalid Username",
                    message: "Your username didn't not match any known admins",
                    delegate: nil,
                    cancelButtonTitle: "Okay")
                alert.show()
            } else if password != valPassword {
                let alert: UIAlertView = UIAlertView(
                    title: "Invalid Password",
                    message: "Your password was incorrect for the given username",
                    delegate: nil,
                    cancelButtonTitle: "Okay")
                alert.show()
            } else {
                self.performSegueWithIdentifier("goToAdminConsole", sender: self)
            }
        } else {
            // Verify the username and password length.
            if countElements(username) < 6 {
                let alert: UIAlertView = UIAlertView(
                    title: "Invalid Username",
                    message: "Your username must contain at least 6 characters",
                    delegate: nil,
                    cancelButtonTitle: "Okay")
                alert.show()
            } else if countElements(password) < 8 {
                let alert: UIAlertView = UIAlertView(
                    title: "Invalid Password",
                    message: "Your password must contain at least 8 characters",
                    delegate: nil,
                    cancelButtonTitle: "Okay")
                alert.show()
            } else {
                defaults.setObject(username, forKey: DEFAULTS_USERNAME)
                defaults.setObject(password, forKey: DEFAULTS_PASSWORD)
                self.performSegueWithIdentifier("goToAdminConsole", sender: self)
            }
        }
    }
}
