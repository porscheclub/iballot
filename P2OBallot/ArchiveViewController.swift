//
//  ArchiveViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/19/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class ArchiveViewController: UIViewController {
    
    @IBOutlet weak var newContestButton: UIButton!
    @IBOutlet weak var editContestButton: UIButton!
    
    var editSelected: Bool = false
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showNewContestPrompt" && editSelected {
            self.editSelected = false
            let navController: UINavigationController = segue.destinationViewController as UINavigationController
            let controller: NewContestViewController = navController.topViewController as NewContestViewController
            
            // Pull the current active competition.
            let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            let context: NSManagedObjectContext! = appDelegate.managedObjectContext
            let request: NSFetchRequest = NSFetchRequest()
            let description: NSEntityDescription! = NSEntityDescription.entityForName("Competition", inManagedObjectContext: context)
            let predicate: NSPredicate! = NSPredicate(format: "isActive == %@", NSNumber(bool: true))
            request.entity = description
            request.predicate = predicate
            var error: NSError?
            let result: NSArray! = context.executeFetchRequest(request, error: &error)
            let competition: Competition = result.firstObject! as Competition
            let cars: NSArray = competition.cars.sortedArrayUsingDescriptors([NSSortDescriptor(key: "number", ascending: true)])
            
            controller.competition = competition
            controller.carMin = (cars.firstObject! as Car).number.integerValue
            controller.carMax = (cars.lastObject! as Car).number.integerValue
        }
    }
    
    @IBAction func onNewContestSelected(sender: AnyObject) {
        self.performSegueWithIdentifier("showNewContestPrompt", sender: self)
    }
    
    @IBAction func onEditContestSelected(sender: AnyObject) {
        self.editSelected = true
        self.performSegueWithIdentifier("showNewContestPrompt", sender: self)
    }
}
