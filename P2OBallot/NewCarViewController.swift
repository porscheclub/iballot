//
//  NewCarViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class NewCarViewController: FXFormViewController {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext?
    var competition: Competition?
    var car: Car!
    
    var myForm: NewCarForm = NewCarForm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formController.form = myForm
    }
    
    override func viewWillAppear(animated: Bool) {
        
        // If there's an old car, then we show its info.
        if let oldCar = self.car {
            myForm.ownerFirstName = oldCar.firstName
            myForm.ownerLastName = oldCar.lastName
            myForm.model = oldCar.model
            myForm.year = oldCar.year?.stringValue
        }
        
        super.viewWillAppear(animated)
    }
    
    @IBAction func onCancelSelected(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onSaveSelected(sender: AnyObject) {
        // Validate the data.
        if myForm.year != nil && (myForm.year?.toInt() == nil || myForm.year.toInt() < 1920 || myForm.year.toInt() > 3000) {
            let alert: UIAlertController = UIAlertController(
                title: "Invalid Year",
                message: "The year you entered is invalid.",
                preferredStyle: .Alert)
            let action = UIAlertAction(title: "Okay", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            // The car already exists, delete everything but the number.
            self.car.firstName = myForm.ownerFirstName
            self.car.lastName = myForm.ownerLastName
            self.car.model = myForm.model
        
            if let year = myForm.year {
                self.car.year = year.toInt()
            } else {
                self.car.year = nil
            }

            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
