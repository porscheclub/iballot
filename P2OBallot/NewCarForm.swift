//
//  NewCarForm.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

class NewCarForm: NSObject, FXForm {
    
    var ownerFirstName: String!
    var ownerLastName: String!
    var model: String!
    var year: String!
    
    func fields() -> NSArray {
        return NSArray(array: ["ownerFirstName", "ownerLastName", "model", "year"])
    }
    
    func ownerFirstNameField() -> NSDictionary {
        return NSDictionary(dictionary: [ FXFormFieldTitle: NSString(string: "First Name") ])
    }
    
    func ownerLastNameField() -> NSDictionary {
        return NSDictionary(dictionary: [ FXFormFieldTitle: NSString(string: "Last Name") ])
    }
    
    func modelField() -> NSDictionary {
        return NSDictionary(dictionary: [
            FXFormFieldHeader: NSString(string: "Car Info"),
            FXFormFieldTitle: NSString(string: "Model")])
    }
}
