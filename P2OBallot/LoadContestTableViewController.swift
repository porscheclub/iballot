//
//  LoadContestTableViewController.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/8/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import UIKit
import CoreData

class LoadContestTableViewController: UITableViewController, UITableViewDelegate {

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var context: NSManagedObjectContext!
    var competitions: NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        self.context = appDelegate.managedObjectContext
        
        // Retrieve the competitions
        let description: NSEntityDescription = NSEntityDescription.entityForName(
            "Competition", inManagedObjectContext: self.context)!
        let request: NSFetchRequest = NSFetchRequest()
        request.entity = description
        var err: NSError?
        self.competitions = self.context.executeFetchRequest(request, error: &err)
        if let error = err {
            println("\(error)")
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.competitions.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("competitionReuse") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: "competitionReuse")
        }

        let competition: Competition = self.competitions.objectAtIndex(indexPath.row) as Competition
        cell.textLabel?.text = "\(competition.name)"
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        cell.detailTextLabel?.text = "\(competition.location) - \(formatter.stringFromDate(competition.startDate))"
        if Bool(competition.isActive) { cell.accessoryType = .Checkmark }

        return cell
    }

    // MARK: - Events
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let pred: NSPredicate = NSPredicate(format: "isActive == %@", NSNumber(bool: true))!
        let currentActive: NSArray = self.competitions.filteredArrayUsingPredicate(pred)
        for obj in currentActive {
            (obj as Competition).isActive = false
            self.tableView.reloadRowsAtIndexPaths(
                [NSIndexPath(forRow: self.competitions.indexOfObject(obj as Competition), inSection: 0)],
                withRowAnimation: .None)
        }
        
        (self.competitions.objectAtIndex(indexPath.row) as Competition).isActive = true
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
    }

    @IBAction func onCancelSelected(sender: AnyObject) {
        self.context.rollback()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onDoneSelected(sender: AnyObject) {
        var error: NSError?
        self.context.save(&error)
        if let err = error {
            println("\(err)")
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
