//
//  NewContestForm.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 12/24/14.
//  Copyright (c) 2014 MORPCA. All rights reserved.
//

class NewContestForm: NSObject, FXForm {
    
    var carMin: String!
    var carMax: String!
    var competitionName: String!
    var location: String!
    var about: String!
    var hasCsv: Bool = false
    
    var startDate: NSDate!
    var endDate: NSDate!
    
    func fields() -> NSArray {
        return NSArray(array: ["competitionName", "location", "about", "hasCsv", "carMin", "carMax"])
    }
    
    func competitionNameField() -> NSDictionary {
        return NSDictionary(dictionary: [FXFormFieldHeader: NSString(string: "Overview")])
    }
    
    func aboutField() -> NSDictionary {
        return NSDictionary(dictionary: [FXFormFieldType: FXFormFieldTypeLongText])
    }
    
    func hasCsvField() -> NSDictionary {
        return NSDictionary(dictionary: [
            FXFormFieldHeader: NSString(string: "Pre-Loaded Data"),
            FXFormFieldTitle: NSString(string: "I have a CSV file")])
    }
    
    func carMinField() -> NSDictionary {
        return NSDictionary(dictionary: [
            FXFormFieldTitle: NSString(string: "Minimum Car Number")])
    }
    
    func carMaxField() -> NSDictionary {
        return NSDictionary(dictionary: [
            FXFormFieldTitle: NSString(string: "Maximum Car Number")])
    }
}
