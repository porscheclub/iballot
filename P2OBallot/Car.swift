//
//  Car.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/1/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import Foundation
import CoreData

@objc(Car)
class Car: NSManagedObject {

    @NSManaged var firstName: String?
    @NSManaged var lastName: String?
    @NSManaged var model: String?
    @NSManaged var number: NSNumber
    @NSManaged var year: NSNumber?
    @NSManaged var competition: Competition!
    @NSManaged var votes: NSSet

}
