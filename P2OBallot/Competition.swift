//
//  Competition.swift
//  P2OBallot
//
//  Created by Daniel Marchese on 1/1/15.
//  Copyright (c) 2015 MORPCA. All rights reserved.
//

import Foundation
import CoreData

@objc(Competition)
class Competition: NSManagedObject {

    @NSManaged var about: String?
    @NSManaged var endDate: NSDate?
    @NSManaged var isActive: NSNumber
    @NSManaged var location: String
    @NSManaged var name: String
    @NSManaged var startDate: NSDate
    @NSManaged var cars: NSSet
    @NSManaged var votes: NSSet

}
